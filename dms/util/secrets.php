<?php

use Aws\SecretsManager\SecretsManagerClient;

function secret(string $name): string
{
    $client = new SecretsManagerClient([
        'version' => '2017-10-17',
        'region' => env('AWS_REGION', env('AWS_DEFAULT_REGION'))
    ]);

    $result = $client->getSecretValue(['SecretId' => $name]);
    return $result["SecretString"] ?? base64_decode($result['SecretBinary']);
}
