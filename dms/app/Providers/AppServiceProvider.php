<?php

namespace App\Providers;

use App\AppCms;
use App\AppOrm;
use App\Domain\Services\Persistence\IAnalysisRepository;
use App\Domain\Services\Persistence\IDocumentRepository;
use App\Infrastructure\Persistence\DbAnalysisRepository;
use App\Infrastructure\Persistence\DbDocumentRepository;
use Dms\Core\ICms;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($_SERVER['LAMBDA_TASK_ROOT'] ?? false) {
            URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IOrm::class, AppOrm::class);
        $this->app->singleton(ICms::class, AppCms::class);

        $this->app->singleton(IDocumentRepository::class, DbDocumentRepository::class);
        $this->app->singleton(IAnalysisRepository::class, DbAnalysisRepository::class);
    }
}
