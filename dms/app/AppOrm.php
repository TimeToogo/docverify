<?php declare(strict_types = 1);

namespace App;

use App\Domain\Entities\Analysis;
use App\Domain\Entities\AnalysisScore;
use App\Domain\Entities\Document;
use App\Infrastructure\Persistence\AnalysisMapper;
use App\Infrastructure\Persistence\AnalysisScoreMapper;
use App\Infrastructure\Persistence\DocumentMapper;
use Dms\Core\Persistence\Db\Mapping\Definition\Orm\OrmDefinition;
use Dms\Core\Persistence\Db\Mapping\Orm;
use Dms\Web\Laravel\Persistence\Db\DmsOrm;

/**
 * The application's orm.
 */
class AppOrm extends Orm
{
    /**
     * Defines the object mappers registered in the orm.
     *
     * @param OrmDefinition $orm
     *
     * @return void
     */
    protected function define(OrmDefinition $orm)
    {
        $orm->enableLazyLoading();

        $orm->encompass(DmsOrm::inDefaultNamespace());

        $orm->entities([
            Document::class => DocumentMapper::class,
            Analysis::class => AnalysisMapper::class,
        ]);

        $orm->valueObjects([
            AnalysisScore::class => AnalysisScoreMapper::class
        ]);
    }
}