<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Connection\IConnection;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Dms\Core\Persistence\DbRepository;
use App\Domain\Services\Persistence\IDocumentRepository;
use App\Domain\Entities\Document;

/**
 * The database repository implementation for the App\Domain\Entities\Document entity.
 */
class DbDocumentRepository extends DbRepository implements IDocumentRepository
{
    public function __construct(IConnection $connection, IOrm $orm)
    {
        parent::__construct($connection, $orm->getEntityMapper(Document::class));
    }
}