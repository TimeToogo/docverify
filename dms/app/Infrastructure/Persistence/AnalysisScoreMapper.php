<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\IndependentValueObjectMapper;
use App\Domain\Entities\AnalysisScore;


/**
 * The App\Domain\Entities\AnalysisScore value object mapper.
 */
class AnalysisScoreMapper extends IndependentValueObjectMapper
{
    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(AnalysisScore::class);

        $map->property(AnalysisScore::TYPE)->to('type')->asVarchar(255);

        $map->property(AnalysisScore::SCORE)->to('score')->asInt();

        $map->property(AnalysisScore::MESSAGE)->to('message')->asVarchar(255);

        $map->property(AnalysisScore::WEIGHTING)->to('weighting')->asInt();


    }
}