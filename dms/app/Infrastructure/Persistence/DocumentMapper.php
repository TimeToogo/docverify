<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;
use App\Domain\Entities\Document;
use Dms\Common\Structure\DateTime\Persistence\DateTimeMapper;
use Dms\Library\Metadata\Infrastructure\Persistence\MetadataMapper;

/**
 * The App\Domain\Entities\Document entity mapper.
 */
class DocumentMapper extends EntityMapper
{
    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(Document::class);
        $map->toTable('documents');

        $map->idToPrimaryKey('id');

        $map->property(Document::USER_ID)->to('user_id')->asVarchar(255);

        $map->property(Document::DOCUMENT_ID)->to('document_id')->asVarchar(255);

        $map->property(Document::FILE_NAME)->to('file_name')->asVarchar(255);

        $map->property(Document::DOCUMENT_TYPE)->to('document_type')->asVarchar(255);

        $map->property(Document::MIME_TYPE)->to('mime_type')->asVarchar(255);

        $map->embedded(Document::EXPIRES_AT)
            ->using(new DateTimeMapper('expires_at'));

        $map->embedded(Document::CREATED_AT)
            ->using(new DateTimeMapper('created_at'));

        $map->embedded(Document::UPDATED_AT)
            ->using(new DateTimeMapper('updated_at'));

        $map->property(Document::DOCUMENT_SOURCE)->to('document_source')->asVarchar(255);

        $map->enum(Document::COUNTRY)->to('country')->asVarchar(2);

        $map->property(Document::S3_URI)->to('s3_uri')->asVarchar(255);

        $map->property(Document::UPLOAD_STATUS)->to('upload_status')->asVarchar(255);

        MetadataMapper::mapMetadataToJsonColumn($map, 'metadata');
    }
}