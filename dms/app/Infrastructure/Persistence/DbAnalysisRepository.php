<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Connection\IConnection;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Dms\Core\Persistence\DbRepository;
use App\Domain\Services\Persistence\IAnalysisRepository;
use App\Domain\Entities\Analysis;

/**
 * The database repository implementation for the App\Domain\Entities\Analysis entity.
 */
class DbAnalysisRepository extends DbRepository implements IAnalysisRepository
{
    public function __construct(IConnection $connection, IOrm $orm)
    {
        parent::__construct($connection, $orm->getEntityMapper(Analysis::class));
    }
}