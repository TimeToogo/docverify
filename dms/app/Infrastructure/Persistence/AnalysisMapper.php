<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;
use App\Domain\Entities\Analysis;
use App\Domain\Entities\Document;
use Dms\Common\Structure\Web\Persistence\UrlMapper;
use App\Infrastructure\Persistence\AnalysisScoreMapper;
use Dms\Common\Structure\DateTime\Persistence\DateTimeMapper;
use ArrayObject;

/**
 * The App\Domain\Entities\Analysis entity mapper.
 */
class AnalysisMapper extends EntityMapper
{
    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(Analysis::class);
        $map->toTable('analyses');

        $map->idToPrimaryKey('id');

        $map->column('document_id')->asUnsignedInt();
        $map->relation(Analysis::DOCUMENT)
            ->to(Document::class)
            ->manyToOne()
            ->withRelatedIdAs('document_id');

        $map->embedded(Analysis::CALLBACK_URL)
            ->withIssetColumn('callback_url')
            ->using(new UrlMapper('callback_url'));

        $map->property(Analysis::CALLBACK_MAX_ATTEMPTS)->to('callback_max_attempts')->nullable()->asInt();

        $map->property(Analysis::CALLBACK_HTTP_STATUS)->to('callback_http_status')->nullable()->asInt();

        $map->property(Analysis::CALLBACK_HTTP_HEADERS)
        ->mappedVia(
            fn ($obj) => json_encode($obj),
            fn ($json) => $json && is_array(json_decode($json, true)) ? new ArrayObject(json_decode($json, true)) : null
        )
        ->to('callback_headers')->nullable()->asMediumText();

        $map->property(Analysis::CALLBACK_HTTP_BODY)->to('callback_http_body')->nullable()->asMediumText();

        $map->property(Analysis::CALLBACK_ERROR)->to('callback_error')->nullable()->asVarchar(255);

        $map->property(Analysis::STATUS)->to('status')->asVarchar(255);

        $map->property(Analysis::S3_REPORT_URI)->to('s3_report_uri')->nullable()->asVarchar(255);

        $map->property(Analysis::S3_HEATMAP_URI)->to('s3_heatmap_uri')->nullable()->asVarchar(255);

        $map->property(Analysis::OVERALL_SCORE)->to('overall_score')->nullable()->asInt();

        $map->property(Analysis::OVERALL_MESSAGE)->to('overall_message')->nullable()->asVarchar(255);

        $map->embeddedCollection(Analysis::SCORES)
            ->toTable('analysis_scores')
            ->withPrimaryKey('id')
            ->withForeignKeyToParentAs('analysis_id')
            ->using(new AnalysisScoreMapper());

        $map->embedded(Analysis::EXPIRES_AT)
            ->using(new DateTimeMapper('expires_at'));

        $map->embedded(Analysis::CREATED_AT)
            ->using(new DateTimeMapper('created_at'));

        $map->embedded(Analysis::UPDATED_AT)
            ->using(new DateTimeMapper('updated_at'));
    }
}