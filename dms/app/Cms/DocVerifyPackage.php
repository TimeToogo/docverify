<?php declare(strict_types = 1);

namespace App\Cms;

use Dms\Core\Package\Definition\PackageDefinition;
use Dms\Core\Package\Package;
use App\Cms\Modules\DocumentModule;

/**
 * The docverify package.
 */
class DocVerifyPackage extends Package
{
    /**
     * Defines the structure of this cms package.
     *
     * @param PackageDefinition $package
     *
     * @return void
     */
    protected function define(PackageDefinition $package)
    {
        $package->name('docverify');

        $package->metadata([
            'icon' => 'binoculars',
        ]);

        $package->modules([
            'document' => DocumentModule::class,
        ]);
    }
}