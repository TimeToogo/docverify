<?php declare(strict_types = 1);

namespace App\Cms\Modules;

use Dms\Core\Auth\IAuthSystem;
use Dms\Core\Common\Crud\CrudModule;
use Dms\Core\Common\Crud\Definition\CrudModuleDefinition;
use Dms\Core\Common\Crud\Definition\Form\CrudFormDefinition;
use Dms\Core\Common\Crud\Definition\Table\SummaryTableDefinition;
use Dms\Core\File\IUploadedFile;
use Dms\Core\File\IFile;
use App\Domain\Services\Persistence\IDocumentRepository;
use App\Domain\Entities\Document;
use App\Domain\Services\Persistence\IAnalysisRepository;
use Dms\Common\Structure\Field;
use Dms\Common\Structure\Geo\Country;
use Dms\Common\Structure\DateTime\DateTime;
use Dms\Common\Structure\FileSystem\File;
use Dms\Library\Metadata\Domain\ObjectMetadata;
use GuzzleHttp\Psr7\MimeType;
use Ramsey\Uuid\Uuid;

/**
 * The document module.
 */
class DocumentModule extends CrudModule
{
    protected IAnalysisRepository $analysisRepo;

    public function __construct(IDocumentRepository $dataSource, IAuthSystem $authSystem, IAnalysisRepository $analysisRepo)
    {
        $this->analysisRepo = $analysisRepo;
        parent::__construct($dataSource, $authSystem);
    }

    /**
     * Defines the structure of this module.
     *
     * @param CrudModuleDefinition $module
     */
    protected function defineCrudModule(CrudModuleDefinition $module)
    {
        $module->name('document');

        $module->labelObjects()->fromProperty(Document::USER_ID);

        $module->metadata([
            'icon' => 'file-text'
        ]);

        $module->crudForm(function (CrudFormDefinition $form) {
            if ($form->isCreateForm()) {
                $form->onSubmit(function (Document $doc, array $input) {
                    $doc->metadata = new ObjectMetadata();
                    $doc->documentId = Uuid::uuid4()->toString(); 
                    $doc->createdAt = new DateTime(new \DateTime());
                    $doc->updatedAt = new DateTime(new \DateTime());
                });
            }

            $form->section('Details', [
                $form->field(
                    Field::create('user_id', 'User Id')->string()->required()
                )->bindToProperty(Document::USER_ID),
                //
                $form->field(
                    Field::create('document_type', 'Document Type')->string()->required()
                )->bindToProperty(Document::DOCUMENT_TYPE),
                //
                //
                $form->field(
                    Field::create('document_source', 'Document Source')->string()->required()
                )->bindToProperty(Document::DOCUMENT_SOURCE),
                //
                $form->field(
                    Field::create('country', 'Country')->enum(Country::class, Country::getNameMap())->required()
                )->bindToProperty(Document::COUNTRY),
                //
            ]);

            $form->section('File', [
                $form->field(
                    Field::create('file', 'File')->file()->required()
                        ->moveToPathWithCustomFileName("s3://{$_ENV['S3_INPUT_BUCKET']}", function (IUploadedFile $file) {
                            return Uuid::uuid4()->toString() . '.' . $file->getExtension();
                        })
                )->bindToCallbacks(
                    fn (Document $doc): File => new File($doc->s3Uri, $doc->fileName),
                    function (Document $doc, IFile $file) {
                        $doc->s3Uri = $file->getFullPath();
                        $doc->fileName = $file->getClientFileName();
                        $extension = last(explode('.', $file->getClientFileName()));
                        $doc->mimeType = $extension ? MimeType::fromExtension($extension) : 'application/octet-stream';
                        $doc->uploadStatus = 'NEW';
                    }
                ),
            ]);

            if (!$form->isCreateForm()) {
                $form->continueSection([
                    $form->field(
                        Field::create('file_name', 'File Name')->string()->required()->readonly()
                    )->bindToCallbacks(fn ($d) => $d->fileName, fn () => null),
                    //
                    $form->field(
                        Field::create('mime_type', 'Mime Type')->string()->required()->readonly()
                    )->bindToCallbacks(fn ($d) => $d->mimeType, fn () => null),
                    //
                    $form->field(
                        Field::create('upload_status', 'Upload Status')->string()->required()->readonly()
                    )->bindToCallbacks(fn ($d) => $d->uploadStatus, fn () => null)
                ]);
            }

            $form->section('Audit', [
                $form->field(
                    Field::create('expires_at', 'Expires At')->dateTime()->required()
                )->bindToProperty(Document::EXPIRES_AT),
            ]);

            if (!$form->isCreateForm()) {
                $form->continueSection([
                    $form->field(
                        Field::create('created_at', 'Created At')->dateTime()->required()->readonly()
                    )->bindToProperty(Document::CREATED_AT),
                    //
                    $form->field(
                        Field::create('updated_at', 'Updated At')->dateTime()->required()->readonly()
                    )->bindToProperty(Document::UPDATED_AT),
                ]);
            }

            if (!$form->isCreateForm()) {
                $form->dependentOnObject(function (CrudFormDefinition $form, Document $document) {
                    $form->section('Analysis', [
                        $form->field(
                            Field::create('analysis', 'Analysis')->module(
                                new AnalysisModule($this->analysisRepo, $this->getAuthSystem(), $document)
                            )
                        )->withoutBinding()
                    ]);
                });
            }
        });

        $module->removeAction()->deleteFromDataSource();

        $module->summaryTable(function (SummaryTableDefinition $table) {
            $table->mapProperty(Document::USER_ID)->to(Field::create('user_id', 'User Id')->string()->required());
            $table->mapProperty(Document::FILE_NAME)->to(Field::create('file_name', 'File Name')->string()->required());
            $table->mapProperty(Document::DOCUMENT_TYPE)->to(Field::create('document_type', 'Document Type')->string()->required());
            $table->mapProperty(Document::UPLOAD_STATUS)->to(Field::create('upload_status', 'Upload Status')->string()->required());
            $table->mapProperty(Document::EXPIRES_AT)->to(Field::create('expires_at', 'Expires At')->dateTime()->required());
            $table->mapProperty(Document::CREATED_AT)->to(Field::create('created_at', 'Created At')->dateTime()->required());
            $table->mapProperty(Document::UPDATED_AT)->to(Field::create('updated_at', 'Updated At')->dateTime()->required());

            $table->view('all', 'All')
                ->loadAll()
                ->asDefault();
        });
    }
}