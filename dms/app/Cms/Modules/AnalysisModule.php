<?php declare(strict_types = 1);

namespace App\Cms\Modules;

use Dms\Core\Auth\IAuthSystem;
use Dms\Core\Common\Crud\CrudModule;
use Dms\Core\Common\Crud\ICrudModule;
use Dms\Core\Common\Crud\Definition\CrudModuleDefinition;
use Dms\Core\Common\Crud\Definition\Form\CrudFormDefinition;
use Dms\Core\Common\Crud\Definition\Table\SummaryTableDefinition;
use App\Domain\Services\Persistence\IAnalysisRepository;
use App\Domain\Entities\Analysis;
use Dms\Common\Structure\Field;
use App\Domain\Services\Persistence\IDocumentRepository;
use App\Domain\Entities\Document;
use Dms\Common\Structure\DateTime\DateTime;
use Dms\Common\Structure\FileSystem\File;
use App\Domain\Entities\AnalysisScore;
use App\Cms\Modules\Fields\AnalysisScoreField;
use Dms\Core\Model\IMutableObjectSet;

/**
 * The analysis module.
 */
class AnalysisModule extends CrudModule
{
    protected Document $document;

    public function __construct(IAnalysisRepository $dataSource, IAuthSystem $authSystem, Document $document)
    {
        $this->document = $document;
        parent::__construct($dataSource, $authSystem);
    }
    
    protected function loadCrudModuleWithDataSource(IMutableObjectSet $dataSource) : ICrudModule
    {
        return new static($dataSource, $this->authSystem, $this->document);
    } 

    /**
     * Defines the structure of this module.
     *
     * @param CrudModuleDefinition $module
     */
    protected function defineCrudModule(CrudModuleDefinition $module)
    {
        $module->name('analysis');

        $module->labelObjects()->fromCallback(fn ($a) => $a->createdAt->format(\DateTime::ISO8601));

        $module->crudForm(function (CrudFormDefinition $form) {
            if ($form->isCreateForm()) {
                $form->onSubmit(function (Analysis $a) {
                    $a->document = $this->document;
                    $a->createdAt = new DateTime(new \DateTime());
                    $a->scores = AnalysisScore::collection();
                    $a->status = 'NEW';
                });
            }

            $form->onSubmit(function (Analysis $a) {
                $a->updatedAt = new DateTime(new \DateTime());
            });

            if (!$form->isCreateForm()) {
                $form->section('Details', [
                    $form->field(
                        Field::create('status', 'Status')->string()->required()->readonly()
                    )->bindToCallbacks(fn ($a) => $a->status, fn () => null)
                ]);
            }

            if (!$form->isCreateForm()) {
                $form->section('Results', [
                    //
                    $form->field(
                        Field::create('report', 'Report')->file()
                            ->moveToPathWithStaticFileName('', '')
                            ->readonly()
                    )->bindToCallbacks(
                        fn (Analysis $a): File|null => $a->s3ReportUri ? new File($a->s3ReportUri, $a->document->fileName . '-analysis.pdf') : null,
                        fn () => null
                    ),
                    //
                    $form->field(
                        Field::create('heatmap', 'Heatmap')->file()
                            ->moveToPathWithStaticFileName('', '')
                            ->readonly()
                    )->bindToCallbacks(
                        fn (Analysis $a): File|null => $a->s3HeatmapUri ? new File($a->s3HeatmapUri, $a->document->fileName . '-heatmap.png') : null,
                        fn () => null
                    ),
                    //
                    $form->field(
                        Field::create('overall_score', 'Overall Score')->int()->readonly()
                    )->bindToProperty(Analysis::OVERALL_SCORE),
                    //
                    $form->field(
                        Field::create('overall_message', 'Overall Message')->string()->readonly()
                    )->bindToProperty(Analysis::OVERALL_MESSAGE),
                    //
                    $form->field(
                        Field::create('scores', 'Scores')->arrayOfField(
                            new AnalysisScoreField('scores', 'Scores')
                        )->mapToCollection(AnalysisScore::collectionType())->readonly()
                    )->bindToProperty(Analysis::SCORES),
                ]);
            }

            $form->section('Callback', [
                $form->field(
                    Field::create('callback_url', 'Callback Url')->url()
                )->bindToProperty(Analysis::CALLBACK_URL),
            ]);

            if (!$form->isCreateForm()) {
                $form->continueSection([
                    $form->field(
                        Field::create('callback_max_attempts', 'Callback Max Attempts')->int()->readonly()
                    )->bindToProperty(Analysis::CALLBACK_MAX_ATTEMPTS),
                    //
                    $form->field(
                        Field::create('callback_http_status', 'Callback Response Status')->int()->readonly()
                    )->bindToProperty(Analysis::CALLBACK_HTTP_STATUS),
                    //
                    $form->field(
                        Field::create('callback_headers', 'Callback Response Headers')->arrayOf(
                            Field::element()->string()->readonly()
                        )->readonly()
                    )->bindToCallbacks(
                        fn ($a) => $a->callbackHttpHeaders ? array_map(fn ($k, $v) => "$k: $v", array_keys($a->callbackHttpHeaders), $a->callbackHttpHeaders) : [],
                        fn () => null
                    ),
                    //
                    $form->field(
                        Field::create('callback_http_body', 'Callback Response Body')->string()->readonly()
                    )->bindToProperty(Analysis::CALLBACK_HTTP_BODY),
                    //
                    $form->field(
                        Field::create('callback_error', 'Callback Error')->string()->readonly()
                    )->bindToProperty(Analysis::CALLBACK_ERROR),
                ]);
            }

            $form->section('Audit', [
                $form->field(
                    Field::create('expires_at', 'Expires At')->dateTime()->required()
                )->bindToProperty(Analysis::EXPIRES_AT),
                //
            ]);

            if (!$form->isCreateForm()) {
                $form->continueSection([
                    $form->field(
                        Field::create('created_at', 'Created At')->dateTime()->required()->readonly()
                    )->bindToProperty(Analysis::CREATED_AT),
                    //
                    $form->field(
                        Field::create('updated_at', 'Updated At')->dateTime()->required()->readonly()
                    )->bindToProperty(Analysis::UPDATED_AT)
                ]);
            }
        });

        $module->removeAction()->deleteFromDataSource();

        $module->summaryTable(function (SummaryTableDefinition $table) {
            $table->mapProperty(Analysis::CREATED_AT)->to(Field::create('created_at', 'Created At')->dateTime()->required());
            $table->mapProperty(Analysis::STATUS)->to(Field::create('status', 'Status')->string()->required());
            $table->mapProperty(Analysis::CALLBACK_URL)->to(Field::create('callback_url', 'Callback Url')->url()->required());
            $table->mapProperty(Analysis::OVERALL_SCORE)->to(Field::create('overall_score', 'Overall Score')->int());
            $table->mapProperty(Analysis::UPDATED_AT)->to(Field::create('updated_at', 'Updated At')->dateTime()->required());

            $table->view('all', 'All')
                ->loadAll()
                ->asDefault();
        });
    }
}