<?php declare(strict_types = 1);

namespace App\Cms\Modules\Fields;

use Dms\Core\Common\Crud\Definition\Form\ValueObjectFieldDefinition;
use Dms\Core\Common\Crud\Form\ValueObjectField;
use App\Domain\Entities\AnalysisScore;
use Dms\Common\Structure\Field;

/**
 * The App\Domain\Entities\AnalysisScore value object field.
 */
class AnalysisScoreField extends ValueObjectField
{


    public function __construct(string $name, string $label)
    {
        parent::__construct($name, $label);
    }

    /**
     * Defines the structure of this value object field.
     *
     * @param ValueObjectFieldDefinition $form
     *
     * @return void
     */
    protected function define(ValueObjectFieldDefinition $form)
    {
        $form->bindTo(AnalysisScore::class);

        $form->section('Details', [
            $form->field(
                Field::create('type', 'Type')->string()->required()
            )->bindToProperty(AnalysisScore::TYPE),
            //
            $form->field(
                Field::create('score', 'Score')->int()->required()
            )->bindToProperty(AnalysisScore::SCORE),
            //
            $form->field(
                Field::create('message', 'Message')->string()->required()
            )->bindToProperty(AnalysisScore::MESSAGE),
            //
            $form->field(
                Field::create('weighting', 'Weighting')->int()->required()
            )->bindToProperty(AnalysisScore::WEIGHTING),
            //
        ]);

    }
}