<?php declare(strict_types = 1);

namespace App\Domain\Services\Persistence;

use Dms\Core\Model\ICriteria;
use Dms\Core\Model\ISpecification;
use Dms\Core\Persistence\IRepository;
use App\Domain\Entities\Document;

/**
 * The repository for the App\Domain\Entities\Document entity.
 */
interface IDocumentRepository extends IRepository
{
    /**
     * {@inheritDoc}
     *
     * @return Document[]
     */
    public function getAll() : array;

    /**
     * {@inheritDoc}
     *
     * @return Document
     */
    public function get($id);

    /**
     * {@inheritDoc}
     *
     * @return Document[]
     */
    public function getAllById(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Document|null
     */
    public function tryGet($id);

    /**
     * {@inheritDoc}
     *
     * @return Document[]
     */
    public function tryGetAll(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Document[]
     */
    public function matching(ICriteria $criteria) : array;

    /**
     * {@inheritDoc}
     *
     * @return Document[]
     */
    public function satisfying(ISpecification $specification) : array;
}
