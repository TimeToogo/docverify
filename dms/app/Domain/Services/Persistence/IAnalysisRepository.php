<?php declare(strict_types = 1);

namespace App\Domain\Services\Persistence;

use Dms\Core\Model\ICriteria;
use Dms\Core\Model\ISpecification;
use Dms\Core\Persistence\IRepository;
use App\Domain\Entities\Analysis;

/**
 * The repository for the App\Domain\Entities\Analysis entity.
 */
interface IAnalysisRepository extends IRepository
{
    /**
     * {@inheritDoc}
     *
     * @return Analysis[]
     */
    public function getAll() : array;

    /**
     * {@inheritDoc}
     *
     * @return Analysis
     */
    public function get($id);

    /**
     * {@inheritDoc}
     *
     * @return Analysis[]
     */
    public function getAllById(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Analysis|null
     */
    public function tryGet($id);

    /**
     * {@inheritDoc}
     *
     * @return Analysis[]
     */
    public function tryGetAll(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Analysis[]
     */
    public function matching(ICriteria $criteria) : array;

    /**
     * {@inheritDoc}
     *
     * @return Analysis[]
     */
    public function satisfying(ISpecification $specification) : array;
}
