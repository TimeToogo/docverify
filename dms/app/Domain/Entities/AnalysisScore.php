<?php

declare(strict_types=1);

namespace App\Domain\Entities;

use Dms\Core\Model\Object\ValueObject;
use Dms\Core\Model\Object\ClassDefinition;

class AnalysisScore extends ValueObject
{
    const TYPE = 'type';
    const SCORE = 'score';
    const MESSAGE = 'message';
    const WEIGHTING = 'weighting';

    public string $type;
    public int $score;
    public string $message;
    public int $weighting;

    protected function define(ClassDefinition $class)
    {
    }
}
