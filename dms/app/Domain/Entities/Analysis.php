<?php

declare(strict_types=1);

namespace App\Domain\Entities;

use Dms\Core\Model\Object\Entity;
use Dms\Common\Structure\Web\Url;
use Dms\Common\Structure\DateTime\DateTime;
use Dms\Core\Model\Object\ClassDefinition;

class Analysis extends Entity
{
    const DOCUMENT = 'document';
    const CALLBACK_URL =  'callbackUrl';
    const CALLBACK_MAX_ATTEMPTS =  'callbackMaxAttempts';
    const CALLBACK_HTTP_STATUS =  'callbackHttpStatus';
    const CALLBACK_HTTP_HEADERS =  'callbackHttpHeaders';
    const CALLBACK_HTTP_BODY =  'callbackHttpBody';
    const CALLBACK_ERROR =  'callbackError';
    const STATUS =  'status';
    const S3_REPORT_URI =  's3ReportUri';
    const S3_HEATMAP_URI =  's3HeatmapUri';
    const OVERALL_SCORE =  'overallScore';
    const OVERALL_MESSAGE =  'overallMessage';
    const SCORES = 'scores';
    const EXPIRES_AT = 'expiresAt';
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    public Document $document;
    public Url|null $callbackUrl = null;
    public int|null $callbackMaxAttempts = null;
    public int|null $callbackHttpStatus = null;
    public \ArrayObject|null $callbackHttpHeaders = null;
    public string|null $callbackHttpBody = null;
    public string|null $callbackError = null;
    public string $status = 'NEW';
    public string|null $s3ReportUri = null;
    public string|null $s3HeatmapUri = null;
    public int|null $overallScore = null;
    public string|null $overallMessage = null;
    /** 
     * @var ValueObjectCollection
     */
    public $scores;
    public DateTime $expiresAt;
    public DateTime $createdAt;
    public DateTime $updatedAt;

    protected function defineEntity(ClassDefinition $class)
    {
        $class->property($this->scores)->asType(AnalysisScore::collectionType());
    }
}
