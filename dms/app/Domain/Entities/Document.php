<?php

declare(strict_types=1);

namespace App\Domain\Entities;

use Dms\Core\Model\Object\Entity;
use Dms\Common\Structure\DateTime\DateTime;
use Dms\Common\Structure\Geo\Country;
use Dms\Core\Model\Object\ClassDefinition;
use Dms\Library\Metadata\Domain\IHasMetadata;
use Dms\Library\Metadata\Domain\MetadataTrait;

class Document extends Entity implements IHasMetadata
{
    use MetadataTrait;

    const USER_ID = 'userId';
    const DOCUMENT_ID = 'documentId';
    const FILE_NAME = 'fileName';
    const DOCUMENT_TYPE = 'documentType';
    const MIME_TYPE = 'mimeType';
    const EXPIRES_AT = 'expiresAt';
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DOCUMENT_SOURCE = 'documentSource';
    const COUNTRY = 'country';
    const S3_URI = 's3Uri';
    const UPLOAD_STATUS = 'uploadStatus';

    public string $userId;
    public string $documentId;
    public string $fileName;
    public string $documentType;
    public string $mimeType;
    public DateTime $expiresAt;
    public DateTime $createdAt;
    public DateTime $updatedAt;
    public string $documentSource;
    public Country $country;
    public string $s3Uri;
    public string $uploadStatus;

    protected function defineEntity(ClassDefinition $class)
    {
        $this->defineMetadata($class);
    }
}
