<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
);

if ($_ENV['LAMBDA_TASK_ROOT'] ?? false) {
    $_SERVER['HTTPS'] = 'on';
    $app->useStoragePath('/tmp/storage');

    // Serve static files from the public folder if they exist
    if ($_SERVER['PATH_INFO'] ?? false) {
        $resourcePath = __DIR__ . '/../public/' . $_SERVER['PATH_INFO'];
        if (file_exists($resourcePath) && is_file($resourcePath)) {
            http_response_code(200);
            header('Content-Type: ' . (\GuzzleHttp\Psr7\MimeType::fromFilename($resourcePath) ?? mime_content_type($resourcePath)));
            header('Content-Length: ' . filesize($resourcePath));
            readfile($resourcePath);
            exit;
        }
    }
}

// s3:// file handler
$s3Client = new \Aws\S3\S3Client([
    'region' => $_ENV['AWS_REGION'] ?? $_ENV['AWS_DEFAULT_REGION'] ?? null,
    'version' => '2006-03-01',
]);
$s3Client->registerStreamWrapper();

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
