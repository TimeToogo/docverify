<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->string('user_id', 255);
            $table->string('document_id', 255);
            $table->string('file_name', 255);
            $table->string('document_type', 255);
            $table->string('mime_type', 255);
            $table->dateTime('expires_at');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->string('document_source', 255);
            $table->string('country', 2);
            $table->string('s3_uri', 255);
            $table->string('upload_status', 255);
            $table->mediumText('metadata');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
