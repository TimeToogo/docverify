<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnalysis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analyses', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->integer('document_id')->unsigned();
            $table->string('callback_url', 2083)->nullable();
            $table->integer('callback_max_attempts')->nullable();
            $table->integer('callback_http_status')->nullable();
            $table->mediumText('callback_headers')->nullable();
            $table->mediumText('callback_http_body')->nullable();
            $table->string('callback_error', 255)->nullable();
            $table->string('status', 255);
            $table->string('s3_report_uri', 255)->nullable();
            $table->string('s3_heatmap_uri', 255)->nullable();
            $table->integer('overall_score')->nullable();
            $table->string('overall_message', 255)->nullable();
            $table->dateTime('expires_at');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

            $table->index('document_id', 'IDX_AC86883CC33F7837');
        });

        Schema::create('analysis_scores', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->integer('analysis_id')->unsigned();
            $table->string('type', 255);
            $table->integer('score');
            $table->string('message', 255);
            $table->integer('weighting');

            $table->index('analysis_id', 'IDX_BC6C99D57941003F');
        });

        Schema::table('analyses', function (Blueprint $table) {
            $table->foreign('document_id', 'fk_analyses_document_id_documents')
                    ->references('id')
                    ->on('documents')
                    ->onUpdate('cascade');
        });

        Schema::table('analysis_scores', function (Blueprint $table) {
            $table->foreign('analysis_id', 'fk_analysis_scores_analysis_id_analyses')
                    ->references('id')
                    ->on('analyses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('analysis_scores');
        Schema::drop('analyses');
    }
}
