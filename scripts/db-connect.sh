#!/bin/bash

set -e

echo "Connecting to DB in $ENV"

DIR=$(dirname $0)

function cleanup() {
    if [[ ! -z $PORT_FORWARD_PID ]]; then
        echo "Killing port forward"
        kill $PORT_FORWARD_PID
        wait $PORT_FORWARD_PID
        PORT_FORWARD_PID=""
    fi
}
trap cleanup INT EXIT

LOCAL_PORT=65438

$DIR/../cicd/db-pf.sh $LOCAL_PORT &
sleep 5
PORT_FORWARD_PID=$!
echo "PID: $PORT_FORWARD_PID"

echo "Retreiving db password"
export PGUSER="postgres"
export PGPASSWORD=$(aws secretsmanager get-secret-value --secret-id "docverify/$ENV/db-admin-password" --query "SecretString" --output text)
export PGHOST=localhost
export PGPORT=$LOCAL_PORT

echo "Connecting to db"
DB_DMS_PASSWORD=$(aws secretsmanager get-secret-value --secret-id "docverify/$ENV/db-dms-password" --query "SecretString" --output text)
DB_APP_PASSWORD=$(aws secretsmanager get-secret-value --secret-id "docverify/$ENV/db-app-password" --query "SecretString" --output text)

echo "Creating docverify database"
psql docverify