#!/bin/bash

set -e

cd $(dirname $0)/..

echo "Installling deps"

sh -c 'cd dms && composer install --ignore-platform-reqs --no-scripts'
sh -c 'cd dms && npm ci'
sh -c 'cd lambda && npm ci'
sh -c 'cd infra && npm ci'

echo "Done"
