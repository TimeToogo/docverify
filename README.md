DocVerify
=========

DocVerify is a document verification system commissioned for AECC Global and developed by Elliot Levin.

## Architecture

![Overall Architecture](https://lucid.app/publicSegments/view/9272aa74-cfc3-4c79-a3b1-4fe578577532/image.png)
[Full diagram](https://lucid.app/publicSegments/view/9272aa74-cfc3-4c79-a3b1-4fe578577532/image.png)

### Data model

![ERD](https://lucid.app/publicSegments/view/aeddb5a5-ddd8-4256-9fa2-9021f95911ad/image.png)
[Full diagram](https://lucid.app/publicSegments/view/aeddb5a5-ddd8-4256-9fa2-9021f95911ad/image.png)

### Project structure

```
/ 
  |- cicd/ # CI/CD related config
  |- lambda/ # source code for lambda functions
  |- dms/ # source code for DMS admin panel
  |- docs/ # documentation folder
  |- infra/ # infrastructure as code pipeline
  |- scripts/ # bash scripts for misc dev tasks
  |- var/ # runtime operational files for local dev
```

### Tech stack

 - `lambda`: Serverless Framework with Python 3.8 runtime
 - `dms`: PHP 8 / Laravel / DMS
 - `infra`: Typescript / CDK

### Local development setup

```
./scripts/setup.sh
docker-compose up -d
```

### Environments

TODO

### Appendix:

1. Tech Spec: https://docs.google.com/document/d/1SG2SsOT1LYPfLVpAuUGsUkyvcFyNQL6wtD9_dg9Fs3Y/edit?usp=sharing