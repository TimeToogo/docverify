#!/bin/bash

set -e

echo "Initialising DB in $ENV"

DIR=$(dirname $0)

function cleanup() {
    if [[ ! -z $PORT_FORWARD_PID ]]; then
        echo "Killing port forward"
        kill $PORT_FORWARD_PID
        wait $PORT_FORWARD_PID
        PORT_FORWARD_PID=""
    fi
}
trap cleanup INT EXIT

LOCAL_PORT=65431

$DIR/db-pf.sh $LOCAL_PORT &
sleep 5
PORT_FORWARD_PID=$!
echo "PID: $PORT_FORWARD_PID"

echo "Retreiving db password"
export PGUSER="postgres"
export PGPASSWORD=$(aws secretsmanager get-secret-value --secret-id "docverify/$ENV/db-admin-password" --query "SecretString" --output text)
export PGHOST=localhost
export PGPORT=$LOCAL_PORT

DB_DMS_PASSWORD=$(aws secretsmanager get-secret-value --secret-id "docverify/$ENV/db-dms-password" --query "SecretString" --output text)
DB_APP_PASSWORD=$(aws secretsmanager get-secret-value --secret-id "docverify/$ENV/db-app-password" --query "SecretString" --output text)

echo "Creating docverify database"
psql -c "CREATE DATABASE docverify OWNER postgres" || true

echo "Creating dms user"
psql -tc "SELECT 1 FROM pg_user WHERE usename = 'dms'" | grep -q 1 || \
    psql -c "CREATE USER dms WITH PASSWORD '$DB_DMS_PASSWORD'" 

echo "Creating app user"
psql -tc "SELECT 1 FROM pg_user WHERE usename = 'app'" | grep -q 1 || \
    psql -c "CREATE USER app WITH PASSWORD '$DB_APP_PASSWORD'" 

echo "Granting connect perms"
psql -c "GRANT dms TO postgres"
psql -c "GRANT app TO postgres"
psql -c "GRANT ALL PRIVILEGES ON DATABASE docverify TO postgres"
psql -c "GRANT CONNECT ON DATABASE docverify TO dms"
psql -c "GRANT CONNECT ON DATABASE docverify TO app"
psql -c "GRANT USAGE ON SCHEMA public TO postgres" docverify
psql -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO postgres" docverify
psql -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO postgres" docverify
psql -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO dms" docverify
psql -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO dms" docverify
psql -c "GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO app" docverify
psql -c "GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO app" docverify

echo "Done"