#!/bin/bash

set -e

echo "Running on branch: $CI_COMMIT_BRANCH"

case "$CI_COMMIT_BRANCH" in
master)
    export ENV="prod"
    ;;
staging)
    export ENV="staging"
    ;;
develop)
    export ENV="dev"
    ;;
*)
    echo "Unknown branch, aborting..."
    exit 1
    ;;
esac

echo "Env: ${ENV}"
