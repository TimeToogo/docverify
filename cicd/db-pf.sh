# Port forwards from localhost to the RDS instance
#!/bin/bash

LOCAL_PORT=$1

if [[ -z "$ENV" || -z "$LOCAL_PORT" ]]; then
    echo "usage: ENV=dev|staging|prod db-pf.sh [port]"
    exit 1
fi

set -e

echo "Configuring port forward to $ENV RDS"

function cleanup() {
    if [[ ! -z $COMMAND_ID ]]; then
        echo "Killing TCP relay $COMMAND_ID"
        AWS_PAGER="" aws ssm cancel-command --command-id $COMMAND_ID || true
        COMMAND_ID=""
    fi
}
trap cleanup INT EXIT

RDS_HOSTNAME=$(aws cloudformation list-exports --query "Exports[?Name==\`DocVerify-$ENV-RdsHostName\`].Value" --no-paginate --output text)
BASTION_ID=$(aws cloudformation list-exports --query "Exports[?Name==\`DocVerify-$ENV-Bastion\`].Value" --no-paginate --output text)

REMOTE_PORT=65432
RDS_PORT=5432

echo "Starting TCP relay from $BASTION_ID:$REMOTE_PORT to $RDS_HOSTNAME:$RDS_PORT"
RELAY_CMD="socat -d TCP-LISTEN:$REMOTE_PORT,reuseaddr,fork TCP:$RDS_HOSTNAME:$RDS_PORT"
COMMAND_ID=$(aws ssm send-command --instance-ids $BASTION_ID \
                --document-name AWS-RunShellScript --parameters "{\"commands\": [\"$RELAY_CMD\"]}" \
                --timeout 120 \
                --query "Command.CommandId" --output text)
echo "Command: $COMMAND_ID"

echo "Starting port forwarding from localhost:$LOCAL_PORT to $BASTION_ID:$REMOTE_PORT"
aws ssm start-session \
  --target $BASTION_ID \
  --document-name AWS-StartPortForwardingSession \
  --parameters "{\"portNumber\":[\"$REMOTE_PORT\"], \"localPortNumber\":[\"$LOCAL_PORT\"]}"