#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { InfraStack } from '../lib/infra-stack';
import { ENV } from '../etc/env';

const app = new cdk.App();
new InfraStack(app, `DocVerifyStack/${ENV}`, {
  stackName: `docverify-app-${ENV}`,
  env: { account: '534280825829', region: 'ap-southeast-2' },
});
