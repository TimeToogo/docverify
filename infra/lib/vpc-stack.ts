import {
  BastionHostLinux,
  CfnEIP,
  CfnRoute,
  InstanceType,
  Peer,
  Port,
  SecurityGroup,
  SubnetType,
  Vpc,
} from "@aws-cdk/aws-ec2";
import * as cdk from "@aws-cdk/core";
import { CfnOutput } from "@aws-cdk/core";
import { ENV } from "../etc/env";

export class VpcStack extends cdk.Stack {
  public readonly vpc: Vpc;
  public readonly lambdaSecurityGroup: SecurityGroup;
  public readonly bastionSecurityGroup: SecurityGroup;
  public readonly bastion: BastionHostLinux;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    this.vpc = new Vpc(this, "AppVpc", {
      cidr: "10.0.0.0/16",
      maxAzs: 2,
      natGateways: 0,
      subnetConfiguration: [
        { name: "public", subnetType: SubnetType.PUBLIC, cidrMask: 24 },
        {
          name: "private",
          subnetType: SubnetType.PRIVATE_ISOLATED,
          cidrMask: 24,
        },
        {
          name: "isolated",
          subnetType: SubnetType.PRIVATE_ISOLATED,
          cidrMask: 24,
        },
      ],
    });

    for (const subnetGroupName of ["public", "private", "isolated"]) {
      const subnets = this.vpc.selectSubnets({ subnetGroupName }).subnetIds;
      for (let i = 0; i < subnets.length; i++) {
        new CfnOutput(this, `AppVpc-${subnetGroupName}-Subnet${i}`, {
          exportName: `DocVerify-${ENV}-Subnet-${subnetGroupName}-${i + 1}`,
          value: subnets[i],
        });
      }
    }

    this.lambdaSecurityGroup = new SecurityGroup(this, "LambdaSecurityGroup", {
      vpc: this.vpc,
      securityGroupName: `docverify-${ENV}-lambda-sg`,
      allowAllOutbound: true,
    });

    new CfnOutput(this, `AppVpcLambdaSecurityGroup`, {
      exportName: `DocVerify-${ENV}-Lambda-SG`,
      value: this.lambdaSecurityGroup.securityGroupId,
    });

    this.bastionSecurityGroup = new SecurityGroup(
      this,
      "BastionSecurityGroup",
      {
        vpc: this.vpc,
        securityGroupName: `docverify-${ENV}-bastion-sg`,
        allowAllOutbound: true,
      }
    );
    this.bastionSecurityGroup.addIngressRule(
      Peer.ipv4(this.vpc.vpcCidrBlock),
      Port.allTraffic(),
      "Allow local traffic"
    );

    this.bastion = new BastionHostLinux(this, "BastionHost", {
      instanceName: `docverify-${ENV}-bastion`,
      vpc: this.vpc,
      subnetSelection: { subnetType: SubnetType.PUBLIC },
      instanceType: new InstanceType("t4g.nano"),
      securityGroup: this.bastionSecurityGroup,
    });

    const elasticIp = new CfnEIP(this, "BastionElasticIp", {
      domain: "vpc",
      instanceId: this.bastion.instanceId,
    });

    // Configure as NAT instance
    this.bastion.instance.instance.sourceDestCheck = false
    this.bastion.instance.addUserData(
      `sudo sysctl -w net.ipv4.ip_forward=1`,
      `echo "net.ipv4.ip_forward = 1" | sudo tee /etc/sysctl.d/nat.conf`,
      `sudo /sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE`,
      `sudo yum install -y socat iptables-services`,
      `sudo service iptables save`
    );

    // Route private subnet traffic to NAT instance
    const privateSubnets = this.vpc.selectSubnets({
      subnetGroupName: "private",
    }).subnets;
    for (let i = 0; i < privateSubnets.length; i++) {
      new CfnRoute(this, `NatRoute-${i}`, {
        routeTableId: privateSubnets[i].routeTable.routeTableId,
        instanceId: this.bastion.instanceId,
        destinationCidrBlock: '0.0.0.0/0'
      });
    }

    new CfnOutput(this, `AppVpcBastion`, {
      exportName: `DocVerify-${ENV}-Bastion`,
      value: this.bastion.instanceId,
    });
  }
}
