import * as cdk from "@aws-cdk/core";
import { ENV } from "../etc/env";
import { CognitoStack } from "./cognito-stack";
import { KmsStack } from "./kms-stack";
import { RdsStack } from "./rds-stack";
import { S3Stack } from "./s3-stack";
import { SecretsStack } from "./secrets-stack";
import { VpcStack } from "./vpc-stack";

export class InfraStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const kms = new KmsStack(this, "KmsStack", {
      stackName: `docverify-${ENV}-kms`,
    });
    const vpc = new VpcStack(this, "VpcStack", {
      stackName: `docverify-${ENV}-vpc`,
    });
    const secrets = new SecretsStack(this, "SecretsStack", {
      stackName: `docverify-${ENV}-secrets`,
      key: kms.key,
    });
    const cognito = new CognitoStack(this, "CognitoStack", {
      stackName: `docverify-${ENV}-cognito`,
    });

    const s3 = new S3Stack(this, "S3Stack", {
      stackName: `docverify-${ENV}-s3`,
      key: kms.key,
    });

    const rds = new RdsStack(this, "RdsStack", {
      stackName: `docverify-${ENV}-rds`,
      vpc: vpc.vpc,
      key: kms.key,
      dbPassword: secrets.dbAdminPassword.secretValue,
    });
  }
}
