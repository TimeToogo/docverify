import { UserPool } from "@aws-cdk/aws-cognito";
import * as cdk from "@aws-cdk/core";
import { ENV } from "../etc/env";

export class CognitoStack extends cdk.Stack {
    public readonly userPool: UserPool

    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        this.userPool = new UserPool(this, 'AppUserPool', {
            userPoolName: `docverify-${ENV}-app`
        })
    }
}
