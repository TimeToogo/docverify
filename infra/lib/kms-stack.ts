import { SubnetType, Vpc } from "@aws-cdk/aws-ec2";
import { Key } from "@aws-cdk/aws-kms";
import * as cdk from "@aws-cdk/core";
import { CfnOutput } from "@aws-cdk/core";
import { ENV } from "../etc/env";

export class KmsStack extends cdk.Stack {
  public readonly key: Key;

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    this.key = new Key(this, "AppKey", {
      enableKeyRotation: true,
      alias: `docverify/${ENV}/app`,
    });

    new CfnOutput(this, "KmsKeyOutput", {
      exportName: `DocVerify-${ENV}-KmsKeyArn`,
      value: this.key.keyArn,
    });
  }
}
