import { Key } from "@aws-cdk/aws-kms";
import { Secret } from "@aws-cdk/aws-secretsmanager";
import * as cdk from "@aws-cdk/core";
import { CfnOutput } from "@aws-cdk/core";
import { ENV } from "../etc/env";

interface StackProps {
  key: Key;
}

export class SecretsStack extends cdk.Stack {
  public readonly dbAdminPassword: Secret;
  public readonly dbDmsPassword: Secret;
  public readonly dbAppPassword: Secret;

  public readonly dmsEncryptionKey: Secret;

  constructor(
    scope: cdk.Construct,
    id: string,
    props: cdk.StackProps & StackProps
  ) {
    super(scope, id, props);

    this.dbAdminPassword = new Secret(this, "DbAdminPasswordSecret", {
      secretName: `docverify/${ENV}/db-admin-password`,
      encryptionKey: props.key,
      generateSecretString: { passwordLength: 32, excludePunctuation: true },
    });

    this.dbDmsPassword = new Secret(this, "DbDmsPasswordSecret", {
      secretName: `docverify/${ENV}/db-dms-password`,
      encryptionKey: props.key,
      generateSecretString: { passwordLength: 32, excludePunctuation: true },
    });

    this.dbAppPassword = new Secret(this, "DbAppPasswordSecret", {
      secretName: `docverify/${ENV}/db-app-password`,
      encryptionKey: props.key,
      generateSecretString: { passwordLength: 32, excludePunctuation: true },
    });

    this.dmsEncryptionKey = new Secret(this, "DmsEncryptionKeySecret", {
      secretName: `docverify/${ENV}/dms-encryption-key`,
      encryptionKey: props.key,
      generateSecretString: { passwordLength: 32, excludePunctuation: true },
    });

    new CfnOutput(this, "DbAdminPasswordSecretOutput", {
      exportName: `DocVerify-${ENV}-DbAdminPasswordSecretArn`,
      value: this.dbAdminPassword.secretArn,
    });

    new CfnOutput(this, "DbDmsPasswordSecretOutput", {
      exportName: `DocVerify-${ENV}-DbDmsPasswordSecretArn`,
      value: this.dbDmsPassword.secretArn,
    });

    new CfnOutput(this, "DbAppPasswordSecretOutput", {
      exportName: `DocVerify-${ENV}-DbAppPasswordSecretArn`,
      value: this.dbAppPassword.secretArn,
    });

    new CfnOutput(this, "DmsEncryptionKeyOutput", {
      exportName: `DocVerify-${ENV}-DmsEncryptionKeySecretArn`,
      value: this.dmsEncryptionKey.secretArn,
    });
  }
}
