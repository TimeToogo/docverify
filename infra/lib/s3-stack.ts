import { Key } from "@aws-cdk/aws-kms";
import { Bucket, BucketEncryption } from "@aws-cdk/aws-s3";
import { Secret } from "@aws-cdk/aws-secretsmanager";
import * as cdk from "@aws-cdk/core";
import { CfnOutput } from "@aws-cdk/core";
import { ENV } from "../etc/env";

interface StackProps {
  key: Key;
}

export class S3Stack extends cdk.Stack {
  public readonly inputBucket: Bucket;
  public readonly outputBucket: Bucket;
  public readonly tmpBucket: Bucket;

  constructor(
    scope: cdk.Construct,
    id: string,
    props: cdk.StackProps & StackProps
  ) {
    super(scope, id, props);

    this.inputBucket = new Bucket(this, "InputBucket", {
      bucketName: `docverify-${ENV}-input`,
      encryption: BucketEncryption.KMS,
      encryptionKey: props.key,
    });

    this.outputBucket = new Bucket(this, "OutputBucket", {
      bucketName: `docverify-${ENV}-output`,
      encryption: BucketEncryption.KMS,
      encryptionKey: props.key,
    });

    this.tmpBucket = new Bucket(this, "TempBucket", {
      bucketName: `docverify-${ENV}-tmp`,
      encryption: BucketEncryption.KMS,
      encryptionKey: props.key,
    });

    new CfnOutput(this, "S3InputBucketOutputArn", {
      exportName: `DocVerify-${ENV}-S3InputBucketArn`,
      value: this.inputBucket.bucketArn,
    });

    new CfnOutput(this, "S3OutputBucketOutputArn", {
      exportName: `DocVerify-${ENV}-S3OutputBucketArn`,
      value: this.outputBucket.bucketArn,
    });

    new CfnOutput(this, "S3TempBucketTempArn", {
      exportName: `DocVerify-${ENV}-S3TempBucketArn`,
      value: this.tmpBucket.bucketArn,
    });

    new CfnOutput(this, "S3InputBucketOutputName", {
      exportName: `DocVerify-${ENV}-S3InputBucketName`,
      value: this.inputBucket.bucketName,
    });

    new CfnOutput(this, "S3OutputBucketOutputName", {
      exportName: `DocVerify-${ENV}-S3OutputBucketName`,
      value: this.outputBucket.bucketName,
    });

    new CfnOutput(this, "S3TempBucketTempName", {
      exportName: `DocVerify-${ENV}-S3TempBucketName`,
      value: this.tmpBucket.bucketName,
    });
  }
}
