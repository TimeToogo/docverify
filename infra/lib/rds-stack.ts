import { Peer, Port, SecurityGroup, SubnetType, Vpc } from "@aws-cdk/aws-ec2";
import { Key } from "@aws-cdk/aws-kms";
import {
  AuroraCapacityUnit,
  DatabaseCluster,
  DatabaseClusterEngine,
  ParameterGroup,
  ServerlessCluster,
} from "@aws-cdk/aws-rds";
import * as cdk from "@aws-cdk/core";
import { CfnOutput, Duration, SecretValue } from "@aws-cdk/core";
import { ENV } from "../etc/env";

interface StackProps {
  vpc: Vpc;
  key: Key;
  dbPassword: SecretValue;
}

export class RdsStack extends cdk.Stack {
  public readonly db: ServerlessCluster;
  public readonly securityGroup: SecurityGroup;

  constructor(
    scope: cdk.Construct,
    id: string,
    props: cdk.StackProps & StackProps
  ) {
    super(scope, id, props);

    this.securityGroup = new SecurityGroup(this, "RdsSecurityGroup", {
      securityGroupName: `DocVerify-${ENV}-Rds-SG`,
      vpc: props.vpc,
    });
    this.securityGroup.addIngressRule(
      Peer.ipv4(props.vpc.vpcCidrBlock),
      Port.tcp(5432),
      "VPC Ingress"
    );

    this.db = new ServerlessCluster(this, "AppDatabase", {
      clusterIdentifier: `docverify-${ENV}-db`,
      engine: DatabaseClusterEngine.AURORA_POSTGRESQL,
      parameterGroup: ParameterGroup.fromParameterGroupName(
        this,
        "ParameterGroup",
        "default.aurora-postgresql10"
      ),
      vpc: props.vpc,
      vpcSubnets: {
        subnetGroupName: 'isolated'
      },
      storageEncryptionKey: props.key,
      scaling: {
        autoPause: ENV === 'prod' ? Duration.seconds(0) : Duration.hours(1),
        minCapacity: AuroraCapacityUnit.ACU_2,
        maxCapacity: AuroraCapacityUnit.ACU_4,
      },
      credentials: {
        username: "postgres",
        password: props.dbPassword,
      },
      securityGroups: [this.securityGroup],
    });

    new CfnOutput(this, "RdsEndpointOutput", {
      exportName: `DocVerify-${ENV}-RdsHostName`,
      value: this.db.clusterEndpoint.hostname,
    });
  }
}
